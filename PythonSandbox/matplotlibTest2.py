import tkinter as tk
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg


root= tk.Tk() 

figure3 = plt.Figure(figsize=(5,4), dpi=100)
ax3 = figure3.add_subplot(111)
ax3.scatter(range(3),[4, 7, 99], color = 'g')
scatter3 = FigureCanvasTkAgg(figure3, root) 
scatter3.get_tk_widget().pack(side=tk.LEFT, fill=tk.BOTH)
# ax3.legend(['Distance']) 
ax3.set_xlabel('Sample number')
ax3.set_ylabel('Measured Distance (cm)')
ax3.set_title('Ultrasonic Sensor Measurements')

root.mainloop()