
# Original reference: https://pythonbasics.org/tkinter-canvas/

import tkinter

def drawX(canvas, centerX, centerY, size, width):
    canvas.create_line((centerX - size, centerY - size), (centerX + size, centerY + size), width=width)
    canvas.create_line((centerX - size, centerY + size), (centerX + size, centerY - size), width=width)

# init tk
root = tkinter.Tk()

# create canvas
myCanvas = tkinter.Canvas(root, bg="white", height=300, width=300)

# draw arcs
# coord = 10, 10, 300, 300
# arc = myCanvas.create_arc(coord, start=0, extent=150, fill="red")
# arv2 = myCanvas.create_arc(coord, start=150, extent=215, fill="green")
myCanvas.create_oval(10, 10, 290, 290)
drawX(myCanvas, 30, 40, 10, 4)

# add to window and show
myCanvas.pack()
root.mainloop()