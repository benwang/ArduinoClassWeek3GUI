# Source: https://stackoverflow.com/questions/24214643/python-to-automatically-select-serial-ports-for-arduino

import serial.tools.list_ports
ports = list(serial.tools.list_ports.comports())
for p in ports:
    print(p)

# Print from Windows:
# COM5 - Standard Serial over Bluetooth link (COM5)
# COM4 - Standard Serial over Bluetooth link (COM4)

# Linux:
# /dev/ttyACM0 - ttyACM0