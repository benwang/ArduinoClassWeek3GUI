#!/bin/bash
# Script used to package the application

# Name of script files, without the .py extension
declare -a appName=("Week3GUI" "Week3GUI_MovingAverage")

targetFolder=Week3GUI-Mac

if [ -d $targetFolder ]; then
	echo "$targetFolder already exists. Deleting..."
	rm -r $targetFolder
fi

mkdir $targetFolder

echo "Copying venv over..."
cp -r venv $targetFolder/

echo "Copying scripts..."
scriptFolder=$targetFolder/scripts
mkdir $scriptFolder
for file in ${appName[@]}
do
	cp -v $file.py $scriptFolder
done

echo "Creating launch scripts..."
for file in ${appName[@]}
do
	echo -e "#!/bin/bash\ncd -- \"\$(dirname \"\$BASH_SOURCE\")\"\nsource venv/bin/activate\npython scripts/$file.py" > $targetFolder/$file
	chmod a+x $targetFolder/$file
done

echo "Packaging complete!"
