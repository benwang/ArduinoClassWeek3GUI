# PowerShell run script for Windows

virtualenv venv --python="C:\Program Files\Python37\python.exe"

# Activate session for rest of the script
echo Activating virtual environment ./venv
.\venv\Scripts\activate.ps1

# Install dependencies
echo Installing python packages in requirements.txt...
pip install -r requirements.txt

echo Installation complete

echo ""
echo "Remember to run '.\venv\Scripts\activate.ps1' first!"
