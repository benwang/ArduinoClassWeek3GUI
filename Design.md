# Design

A basic GUI that shows how devices can interface with computer applications. Let's show the current joystick coordinates, show it on a unit circle, and then make a guy move around the screen.

## Resources
- tkinter canvas used for drawing: https://pythonbasics.org/tkinter-canvas/
- PySerial: https://pythonhosted.org/pyserial/shortintro.html
