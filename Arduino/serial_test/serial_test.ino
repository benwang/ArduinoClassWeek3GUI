int x = 0;
int y = 500;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
  
  Serial.println("joystickX:" + String(x) + " joystickY:" + String(y));

  x++;
  y--;
  if (x > 1023)
    x = 0;
  if (y > 1023)
    y = 0;
  if (x < 0)
    x = 1023;
  if (y < 0)
    y = 1023;

  delay(2);
}
