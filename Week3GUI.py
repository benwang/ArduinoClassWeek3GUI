import tkinter as tk
import time

# Serial imports
import sys
import glob
import serial
import serial.tools.list_ports

import re

# Scatter plot imports
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

# Basic example from: https://docs.python.org/3/library/tkinter.html

##########################################
############ Configuration ###############
##########################################
# delay between successive frames in seconds
animationRefreshSeconds = 0.05

# Serial config
BAUD_OPTIONS = [9600, 115200]
DEFAULT_BAUD = 115200
TIMEOUT = 0.05
NUM_FAILURES_ALLOWED = 2

# Joystick config - Serial print must have these tags
X_REGEX_PATTERN = r"joystickX:([0-9]*)"
Y_REGEX_PATTERN = r"joystickY:([0-9]*)"

# Ultrasonic config - Serial print must have these tags
ULTRASONIC_REGEX_PATTERN = r"ultrasonicDistCM:([0-9\.]*)"
ULTRASONIC_MEASUREMENT_LEN = 100

# Configuration for ultrasonic distance plot
ULTRASONIC_MAX_DIST_CM = 250 # cm

##########################################
############### Helpers ##################
##########################################

# Source: https://stackoverflow.com/questions/12090503/listing-available-com-ports-with-python
# Used for obtaining list of serial ports
def serial_ports():
    ports = list(serial.tools.list_ports.comports())
    return ports

class CrossWidget():
    def __init__(self, canvas, xCoord, yCoord, size=10, width=4):
        # Save parameters
        self.canvas = canvas
        self.size = size
        self.width = width

        # Create widgets
        self.line1 = canvas.create_line((xCoord - size, yCoord - size), (xCoord + size, yCoord + size), width=width)
        self.line2 = canvas.create_line((xCoord - size, yCoord + size), (xCoord + size, yCoord - size), width=width)

    def coords(self, xCoord, yCoord):
        self.canvas.coords(self.line1, xCoord - self.size, yCoord - self.size, xCoord + self.size, yCoord + self.size)
        self.canvas.coords(self.line2, xCoord - self.size, yCoord + self.size, xCoord + self.size, yCoord - self.size)

        # print("CrossWidget coords updated to: " + str(xCoord) + ", " + str(yCoord))

class Application(tk.Frame):
    # Definitions
    JOYSTICK_WIDGET_WIDTH = 400
    JOYSTICK_WIDGET_PADDING = 10

        
    # Variables for serial
    availableSerialPorts = []
    serialInitialized = False

    ultrasonicMeasurements = [0] * ULTRASONIC_MEASUREMENT_LEN

    def __init__(self, window=None):
        super().__init__(window)
        self.window = window
        self.window.wm_title("Arduino Class Week 3 GUI")

        # Serial variables
        self.numFailures = 0
        self.serialInitialized = False

        # Create a Tkinter variables for serial
        self.serialPortSelected = tk.StringVar(self.window)
        self.serialBaudSelected = tk.StringVar(self.window)

        # Set default baud rate
        self.serialBaudSelected.set(DEFAULT_BAUD)

        # Joystick widget initialization
        self.crossX = self.JOYSTICK_WIDGET_WIDTH / 2 + self.JOYSTICK_WIDGET_PADDING
        self.crossY = self.JOYSTICK_WIDGET_WIDTH / 2 + self.JOYSTICK_WIDGET_PADDING

        self.statusLabel = "Serial is not initialized"

        # Graphics initialization
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.frame = tk.Frame(self.window)
        
        self.debugLabelText = tk.StringVar(self.window)

        # Refresh serial ports
        self.checkSerialButton = tk.Button(self)
        self.checkSerialButton["text"] = "Detect ports"
        self.checkSerialButton["command"] = self.detectSerialPorts
        self.checkSerialButton.pack(side="top")

        # Detect available serial (requires serial and baud variables to exist)
        self.detectSerialPorts()

        tk.Label(self.window, text="Select serial port:").pack(side="top")
        self.serialPortDropdown.pack(side="top")

        # Dropdown for baud rate
        self.serialBaudDropdown = tk.OptionMenu(self.window, self.serialBaudSelected, *BAUD_OPTIONS)
        tk.Label(self.window, text="Select baud rate:").pack(side="top")
        self.serialBaudDropdown.pack(side="top")

        # link function to change dropdown
        self.serialPortSelected.trace('w', self.openSerialPort)
        self.serialBaudSelected.trace('w', self.openSerialPort)

        self.quit = tk.Button(self, text="QUIT", fg="red",
                              command=self.window.destroy)
        self.quit.pack(side="bottom")

        # Label for printing current Arduino printout
        self.debugLabelText.set("Arduino debug text here")
        self.debugLabel = tk.Label(self.window, textvariable=self.debugLabelText)
        self.debugLabel.pack(side="top")

        # Joystick position widget
        self.myCanvas = tk.Canvas(
            self.window, 
            bg="white", 
            height=(self.JOYSTICK_WIDGET_WIDTH + 2 * self.JOYSTICK_WIDGET_PADDING), 
            width=(self.JOYSTICK_WIDGET_WIDTH + 2 * self.JOYSTICK_WIDGET_PADDING)
        )
        self.myCanvas.create_oval(
            self.JOYSTICK_WIDGET_PADDING,
            self.JOYSTICK_WIDGET_PADDING,
            self.JOYSTICK_WIDGET_PADDING + self.JOYSTICK_WIDGET_WIDTH,
            self.JOYSTICK_WIDGET_PADDING + self.JOYSTICK_WIDGET_WIDTH
        )
        self.crossWidget = CrossWidget(self.myCanvas, self.crossX, self.crossY, 10, 4)

        # Ultrasonic measurement widget
        self.distanceFigure = plt.Figure(figsize=(5,4), dpi=100)
        self.axis = self.distanceFigure.add_subplot(111)
        self.scatterplot, = self.axis.plot(range(-ULTRASONIC_MEASUREMENT_LEN, 0), self.ultrasonicMeasurements, 'r-') #self.axis.scatter([1, 2, 3],[4, 7, 99], color = 'g')
        distanceFigureCanvas = FigureCanvasTkAgg(self.distanceFigure, self.window) 
        distanceFigureCanvas.get_tk_widget().pack(side=tk.RIGHT, fill=tk.BOTH)
        self.axis.set_xlabel('Sample number')
        self.axis.set_ylabel('Measured Distance (cm)')
        self.axis.set_title('Ultrasonic Sensor Measurements')
        self.axis.set_ylim([0, ULTRASONIC_MAX_DIST_CM])

        # add to window and show
        self.myCanvas.pack()
    
    def detectSerialPorts(self):
        self.availableSerialPorts = serial_ports()
        print("Detected the following serial ports: " + str(self.availableSerialPorts))

        if len(self.availableSerialPorts) > 0:
            for port in self.availableSerialPorts:
                print("Checking " + str(port) + " for Arduino")
                print("description: " + str(port.description))
                print("device: " + str(port.device))
                # print("device_path " + str(port.device_path))
                print("hwid: " + str(port.hwid))
                print("interface: " + str(port.interface))
                print("manufacturer: " + str(port.manufacturer))
                print("name: " + str(port.name))
                print("product: " + str(port.product))
                if "Arduino" in str(port.manufacturer):
                    print("Discovered Arduino on port: " + str(port))
                    self.serialPortSelected.set(str(port)) # set the default option
                    self.openSerialPort()
        else:
            self.availableSerialPorts = [""]

        self.serialPortDropdown = tk.OptionMenu(self.window, self.serialPortSelected, *self.availableSerialPorts)
    
    def openSerialPort(self, *args):
        port = self.serialPortSelected.get()
        baud = self.serialBaudSelected.get()
        print("Opening selected serial port: " + port + " at baud " + baud)

        # Close any existing serial
        if self.serialInitialized:
            self.ser.close()
            self.serialInitialized = False

        # Need to turn each port into a string for comparison
        for availablePort in self.availableSerialPorts:
            if str(availablePort) == port:
                print("openSerialPort(): Found selected serial port in available ports: " + port)
                try:
                    self.ser = serial.Serial(availablePort.device, int(baud), timeout=TIMEOUT)
                    self.serialInitialized = True
                    self.numFailures = 0
                except Exception as e:
                    err = "Error opening serial port: " + str(e)
                    print(err)
                    self.debugLabelText.set(err)

    # Normalizes a coordinate 0-1023 to the specified joystick widget width
    def normCoord(self, coord, invert=False):
        if invert:
            coord = 1023 - coord
            
        return coord / 1023.0 * self.JOYSTICK_WIDGET_WIDTH + self.JOYSTICK_WIDGET_PADDING

    def loop(self):
        while True:
            # Update via serial
            if self.serialInitialized:
                # print("Serial is initialized")

                try:
                    # Read line
                    rawLine = self.ser.readline()
                    line = rawLine.decode("utf-8") 
                    self.debugLabelText.set(line.replace("\n", ""))

                    # Parse joystick data
                    xResults = re.search(X_REGEX_PATTERN, line)
                    if xResults:
                        self.crossX = self.normCoord(int(xResults.groups()[0]))
                    yResults = re.search(Y_REGEX_PATTERN, line)
                    if yResults:
                        self.crossY = self.normCoord(int(yResults.groups()[0]), invert=True)

                    # Read ultrasonic sensor data
                    ultrasonicResults = re.search(ULTRASONIC_REGEX_PATTERN, line)
                    if ultrasonicResults:
                        self.ultrasonicMeasurements.append(float(ultrasonicResults.groups()[0]))

                        numMeasurements = len(self.ultrasonicMeasurements)
                        if numMeasurements > ULTRASONIC_MEASUREMENT_LEN:
                            self.ultrasonicMeasurements = self.ultrasonicMeasurements[(numMeasurements - ULTRASONIC_MEASUREMENT_LEN):]
                            numMeasurements = ULTRASONIC_MEASUREMENT_LEN

                        # Update plot
                        # self.axis.scatter(range(numMeasurements), self.ultrasonicMeasurements, color = 'g')
                        # self.scatterplot.set_xdata(range(numMeasurements))
                        self.scatterplot.set_ydata(self.ultrasonicMeasurements)
                        self.distanceFigure.canvas.draw()
                        self.distanceFigure.canvas.flush_events()

                    self.crossWidget.coords(self.crossX, self.crossY)
                except Exception as e:
                    err = "Error reading serial port: " + str(e)
                    print(err)
                    self.debugLabelText.set(err)

                    self.numFailures += 1

                    # Disable serial to prevent further issues after some number of failures
                    if self.numFailures >= NUM_FAILURES_ALLOWED:
                        self.ser.close()
                        self.serialInitialized = False

                        self.statusLabel = "Disabled serial interface after " + str(self.numFailures) + " failures. Check baud rate."

                        self.numFailures = 0
            else:
                self.debugLabelText.set(self.statusLabel)

            # Update graphics
            try:
                self.update_idletasks()
                self.update()
            except:
                break

root = tk.Tk()
app = Application(window=root)
# app.mainloop()
app.loop()